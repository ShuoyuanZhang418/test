import tensorflow as tf
import paiio
import os, json

def get_cluster_info():
    tf_config_json = os.environ.get("TF_CONFIG", "{}")
    print(tf_config_json)

    # 反序列化为python对象
    tf_config = json.loads(tf_config_json)

    # 拿到ClusterSpec的内容
    cluster_spec = tf_config.get("cluster", {})
    cluster_spec_object = tf.train.ClusterSpec(cluster_spec)
    worker_count = cluster_spec_object.num_tasks('worker')

    # 获取角色类型和id， 比如这里的job_name 是 "worker" and task_id 是 0
    task = tf_config.get("task", {})
    task_index = task["index"]    
    return worker_count, task_index

# 设置odps config文件地址
os.environ['ODPS_CONFIG_FILE_PATH']='/mnt/data/dlc_demo/odps_config.ini'
table=["odps://pai_dlc_demo/tables/farmer_train"]

multiworker_strategy = tf.distribute.experimental.MultiWorkerMirroredStrategy()

worker_count, worker_index = get_cluster_info()
#worker_count, worker_index = 1, 0
print('total worker: {0}, worker index: {1}'.format(worker_count, worker_index))
dataset = paiio.data.TableRecordDataset(table,
                                        selected_cols='farmsize, rainfall,  farmincome, claimvalue',
                                        record_defaults=[ 1.0, 1.0, 1.0,  1.0],
                                        slice_id=worker_index,
                                        slice_count=worker_count)

dataset = dataset.map(lambda x1, x2, x3, x4: (tf.stack((x1, x2, x3)), x4))
dataset = dataset.batch(10).repeat(20).prefetch(100)
record = dataset.make_one_shot_iterator().get_next()
print(record)


with multiworker_strategy.scope():
    model = tf.keras.Sequential([tf.keras.layers.Dense(100, input_shape=(3,)),
                                tf.keras.layers.Dense(30, activation=tf.nn.relu),
                                tf.keras.layers.Dense(1)])

    model.compile(loss=tf.keras.losses.MeanAbsoluteError(),
                optimizer=tf.keras.optimizers.Adam(0.01))

history = model.fit(dataset, epochs=10, steps_per_epoch=11)
print(history.history)
#model.evaluate(dataset)

#if worker_index == 0:
save_path = "/mnt/data_oss/dlc_output_models_worker_" + str(worker_index)
print('To save model in worker:', worker_index, 'save path:', save_path)
tf.saved_model.save(model, save_path)

print('I will exit. Worker:', worker_index)
